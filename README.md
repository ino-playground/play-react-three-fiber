# React-Three-Fiber を試す

## レポジトリ

[pmndrs/react-three-fiber: A React renderer for Three.js](https://github.com/pmndrs/react-three-fiber)

## ToDo

[pmndrs/react-xr: 🤳 VR/AR with react-three-fiber](https://github.com/pmndrs/react-xr)

## 参考

- [React Three Fiber Documentation](https://docs.pmnd.rs/react-three-fiber/)
- [Basic demo - CodeSandbox](https://codesandbox.io/s/basic-demo-rrppl0y8l4)
- [Sky dome with annotations - CodeSandbox](https://codesandbox.io/s/sky-dome-with-annotations-wbrfs)
- [Mixing controls - CodeSandbox](https://codesandbox.io/s/mixing-controls-hf1cs)
- [React (Typescript) + react-three-fiber + three-vrm で VRM モデルを表示してみる - Qiita](https://qiita.com/sskmy1024y/items/949bd7ac26ea06a6ab9e)
- [React で Three.js と shader を触りたいのでサンプルを読みほぐしながら react-three-fiber を理解する回 - Qiita](https://qiita.com/hppRC/items/63039b5b4be316a55ef6)
