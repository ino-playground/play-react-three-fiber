import React, { useRef, useEffect } from "react";
import * as THREE from "three";

export const Floor: React.FC = () => {
  const mesh = useRef<THREE.Mesh>({} as THREE.Mesh);
  useEffect(() => {
    mesh.current.rotation.x = -Math.PI / 2;
  }, []);

  return (
    <mesh ref={mesh} receiveShadow>
      <planeBufferGeometry args={[4, 4]} />
      <meshStandardMaterial color="0xeeeeee" roughness={1.0} metalness={0.0} />
    </mesh>
  );
};
