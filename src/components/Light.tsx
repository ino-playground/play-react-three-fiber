import React from "react";
// import * as THREE from "three";

export const Light: React.FC = () => {
  return (
    <>
      <hemisphereLight args={["#808080", "#606060"]} />
      <directionalLight
        color="#ffffff"
        position={[0, 6, 0]}
        castShadow
        shadow-camera-top={2}
        shadow-camera-bottom={-2}
        shadow-camera-right={2}
        shadow-camera-left={-2}
        shadow-mapSize={[4096, 4096]}
      />
    </>
  );
};
