/* https://github.com/pmndrs/react-three-fiber/issues/27 */
import React, { useRef } from "react";
import {
  extend,
  ReactThreeFiber,
  useThree,
  useFrame,
} from "@react-three/fiber";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { Camera } from "three";

extend({ OrbitControls });

declare global {
  namespace JSX {
    interface IntrinsicElements {
      readonly orbitControls: ReactThreeFiber.Object3DNode<
        OrbitControls,
        typeof OrbitControls
      >;
    }
  }
}

type AOrbitControlsProps = {
  props: ReactThreeFiber.Object3DNode<OrbitControls, typeof OrbitControls>;
  camera: Camera | undefined;
  domElement: HTMLCanvasElement | undefined;
};

export const AOrbitControls: React.FC<AOrbitControlsProps> = ({
  props,
  camera,
  domElement,
}: AOrbitControlsProps) => {
  const {
    camera: tcamera,
    gl: { domElement: tdomElement },
  } = useThree();
  const controls = useRef<OrbitControls>();
  useFrame(() => controls.current?.update());

  return (
    <orbitControls
      {...props}
      ref={controls}
      args={[camera || tcamera, domElement || tdomElement]}
    />
  );
};
