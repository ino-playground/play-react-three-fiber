import React from "react";
import * as THREE from "three";

const RandomGeometries: React.FC = () => {
  const baseGeometries = [
    <boxBufferGeometry args={[0.2, 0.2, 0.2]} />,
    <coneBufferGeometry args={[0.2, 0.2, 64]} />,
    <cylinderBufferGeometry args={[0.2, 0.2, 0.2, 64]} />,
    <icosahedronBufferGeometry args={[0.2, 8]} />,
    <torusBufferGeometry args={[0.2, 0.04, 64, 32]} />,
  ];
  const len = baseGeometries.length;

  return baseGeometries[Math.floor(Math.random() * len)];
};

const SingleGrid: React.FC = () => {
  return (
    <mesh
      position={[
        Math.random() * 4 - 2,
        Math.random() * 2,
        Math.random() * 4 - 2,
      ]}
      rotation={[
        Math.random() * 2 * Math.PI,
        Math.random() * 2 * Math.PI,
        Math.random() * 2 * Math.PI,
      ]}
      scale={
        new THREE.Vector3(
          Math.random() + 0.5,
          Math.random() + 0.5,
          Math.random() + 0.5
        )
      }
      castShadow
      receiveShadow
    >
      <RandomGeometries />
      <meshStandardMaterial
        color={Math.random() * 0xffffff}
        roughness={0.7}
        metalness={0.0}
      />
    </mesh>
  );
};

export const RandomGrids: React.FC = () => {
  const grids = [];
  for (let i = 0; i < 50; i++) {
    const id = i.toString();
    grids.push(<SingleGrid key={id} />);
  }

  return <group>{grids}</group>;
};
