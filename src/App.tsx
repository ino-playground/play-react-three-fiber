import React, { useRef } from "react";
import * as THREE from "three";
import { Canvas } from "@react-three/fiber";
import { PerspectiveCamera } from "@react-three/drei";

import { AOrbitControls, Floor, Light, RandomGrids } from "components";

const App: React.FC = () => {
  const camera = useRef<THREE.Camera>();
  const container = useRef();

  return (
    <Canvas
      style={{
        height: window.innerHeight,
        width: window.innerWidth,
        background: "#808080",
      }}
    >
      <PerspectiveCamera
        ref={camera}
        fov={50}
        aspect={window.innerWidth / window.innerHeight}
        near={0.1}
        far={10}
        position={[0, 1.6, 3]}
      >
        <AOrbitControls
          props={{ target: new THREE.Vector3(0, 1.6, 0) }}
          camera={camera.current}
          domElement={container.current}
        />
        <Light />
        <Floor />

        <RandomGrids />
        {/* <Box position={[-1.2, 0, 0]} />
        <Box position={[1.2, 0, 0]} /> */}
        <gridHelper />
      </PerspectiveCamera>
    </Canvas>
  );
};

export default App;
